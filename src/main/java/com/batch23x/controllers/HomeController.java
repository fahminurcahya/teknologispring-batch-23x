package com.batch23x.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.batch23x.models.CategoryModel;
import com.batch23x.repository.CategoryRepo;


@Controller
public class HomeController {
	
	@Autowired
	private CategoryRepo categoryrepo;

	
	@GetMapping("/")
	public String viewHome() {
		return "index";
	}
	@GetMapping("/category")
	public String viewCategory() {
		return "category";
	}
	
	@GetMapping("/product")
	public String viewProduct() {
		return "product";
	}
	@GetMapping("/variant")
	public ModelAndView viewVariant() {
		ModelAndView view = new ModelAndView("variant");
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category", category);
		return view;
	}
		
}
