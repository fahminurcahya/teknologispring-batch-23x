package com.batch23x.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.batch23x.models.OrderHeader;

public interface OrderHeaderRepo extends PagingAndSortingRepository<OrderHeader,Long> {

	@Override
	@Query("select o from OrderHeader o where  o.isDelete=false")
	Page<OrderHeader> findAll(Pageable pageable);

	@Query(value = "select * from order_header", countQuery = "select * from order_header", nativeQuery = true)
	Page<OrderHeader> findByActive(Pageable pageable);

}
