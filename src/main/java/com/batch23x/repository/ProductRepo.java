package com.batch23x.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.batch23x.models.ProductModel;
import com.batch23x.models.dto.ProductDto;
import com.batch23x.models.dto.ProductVariantDto;
import com.batch23x.models.projection.ProductProj;


public interface ProductRepo extends JpaRepository<ProductModel, Integer>{
	
//	projection
	@Query("select p.prodName as name,"
			+ "p.prodPrice as price, p.prodStock as stock, p.variantId as variantId from ProductModel p")
	List<ProductProj> fetchProduct();
	
	
	//dto
	@Query("select new com.batch23x.models.dto.ProductDto (p.prodName as name,p.prodStock as stock) from ProductModel p")
	List<ProductDto> fetchProductDto();
	
	//page
	@Query(value = "select * from product where variant_id=?1",
		    countQuery = "select count(*) from product where variant_id=?1",
		    nativeQuery = true)
	Page<ProductModel> findByName(Integer varnId, Pageable pageable);

	
	@Query("select new com.batch23x.models.dto.ProductVariantDto(p.id as id," 
			+ " p.prodName as name,p.prodPrice as price,p.prodStock as stock,v.varnName as variant, c.cateName as category) "
			+ " from ProductModel p left join VariantModel v on p.variantId= v.id "
			+ " left join CategoryModel c on c.id= v.categoryId")
	Page<ProductVariantDto> fetchProductVariant(Pageable pageable);


	
	
	
}
