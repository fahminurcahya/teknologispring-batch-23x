package com.batch23x.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.batch23x.models.CategoryModel;

public interface CategoryRepo extends JpaRepository<CategoryModel,Integer> {
	
	@Query("select c from CategoryModel c where c.cateIsActive=?1")
	List<CategoryModel> findByIsActive(String isActive);
	
	@Modifying
	@Query("update from CategoryModel c set c.cateIsActive=?1 where c.id=?2 ")
	int updateCategoryActive(String isActive, Integer cateId);

	
	@Modifying
	@Query("update from CategoryModel c set c.cateIsActive=:isActive where c.id=:cateId ")
	int setCategoryActive(String isActive, Integer cateId);

	@Modifying
	@Query("delete from CategoryModel c  where c.id=:cateId")
	int deleteCategoryJpa(@Param("cateId")  Integer cateId);
}
