package com.batch23x.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.batch23x.models.VariantModel;

public interface VariantRepo extends JpaRepository<VariantModel,Integer> {
	
	@Query("select v from VariantModel v where v.varnIsActive=?1")
	List<VariantModel> findByIsActive(String isActive);
	
	@Modifying
	@Query("update from VariantModel v set v.varnIsActive=?1 where v.id=?2 ")
	int updateVariantActive(String isActive, Integer varnId);

	
	@Modifying
	@Query("update from VariantModel v set v.varnIsActive=:isActive where v.id=:varnId ")
	int setVariantActive(String isActive, Integer varnId);

	@Modifying
	@Query("delete from VariantModel v  where v.id=:varnId")
	int deleteVariantJpa(@Param("varnId")  Integer varnId);
	
	@Query(value="select * from variant v left join category c on v.category_id = c.id where c.id=?1",nativeQuery = true)
	List<VariantModel> findVariantById(Integer id);
	
	@Query(value="select * from variant v left join product p on v.id = p.variant_id where v.id=?1",nativeQuery = true)
	List<VariantModel> findVarian(Integer id);
	
	
	
}
