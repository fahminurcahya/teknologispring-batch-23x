package com.batch23x;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Bath23xApplication {

	public static void main(String[] args) {
		SpringApplication.run(Bath23xApplication.class, args);
	}

}
