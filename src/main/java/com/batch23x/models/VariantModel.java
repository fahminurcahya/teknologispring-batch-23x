package com.batch23x.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="variant")
public class VariantModel {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
	private Integer id;
	
	@Column(name="varn_initial")
	@Size(min=3,max=5)
	private String varnInitial;
	
	@Column(name="varn_name")
	@Size(max=25)
	private String varnName;
	
	@Column(name="varn_is_active")
	@Size(max=3)
	private String varnIsActive;
	
	@Column(name="category_id")
	private Integer categoryId;
	
	
	@ManyToOne
	@JsonBackReference
	@JoinColumn(name="category_id", insertable = false, updatable = false)
	private CategoryModel categoryModel;
	
	@OneToMany(mappedBy="variantModel", cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	@JsonManagedReference
	@Column(nullable=true)
	private Set<ProductModel> prod;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVarnInitial() {
		return varnInitial;
	}

	public void setVarnInitial(String varnInitial) {
		this.varnInitial = varnInitial;
	}

	public String getVarnName() {
		return varnName;
	}

	public void setVarnName(String varnName) {
		this.varnName = varnName;
	}

	public String getVarnIsActive() {
		return varnIsActive;
	}

	public void setVarnIsActive(String varnIsActive) {
		this.varnIsActive = varnIsActive;
	}

	

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	
}
