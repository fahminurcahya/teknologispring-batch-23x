package com.batch23x.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.*;


@Entity
@Getter
@Setter
@Table(name="order_header")
public class OrderHeader extends CommonEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="order_id")
	private Long orderId;
	
	@Column(name="order_ref")
	@Size(max=25)
	private String orderRef;
	
	@Column(name="order_amount")
	private Double amount;
	
	@Column(name="order_is_active",length=1)
	private String isActive;
	
	@OneToMany(mappedBy = "orderHeader", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Column(nullable = true)
	@JsonManagedReference
	private Set<OrderDetail> orderDetails;

	
	


//	public OrderHeader() {
//		
//	}
//	public OrderHeader(Long orderId, @Size(max = 25) String orderRef, Double amount, String isActive,
//			Set<OrderDetail> orderDetails) {
//		super();
//		this.orderId = orderId;
//		this.orderRef = orderRef;
//		this.amount = amount;
//		this.isActive = isActive;
//		this.orderDetails = orderDetails;
//	}
//	
//	
//
//	public Long getOrderId() {
//		return orderId;
//	}
//
//	public void setOrderId(Long orderId) {
//		this.orderId = orderId;
//	}
//
//	public String getOrderRef() {
//		return orderRef;
//	}
//
//	public void setOrderRef(String orderRef) {
//		this.orderRef = orderRef;
//	}
//
//	public Double getAmount() {
//		return amount;
//	}
//
//	public void setAmount(Double amount) {
//		this.amount = amount;
//	}
//
//	public String getIsActive() {
//		return isActive;
//	}
//
//	public void setIsActive(String isActive) {
//		this.isActive = isActive;
//	}
//
//	public Set<OrderDetail> getOrderDetails() {
//		return orderDetails;
//	}
//
//	public void setOrderDetails(Set<OrderDetail> orderDetails) {
//		this.orderDetails = orderDetails;
//	}
	
	
	
	


}
