package com.batch23x.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="category")
public class CategoryModel {
	
		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    @Column(name="id")
	    private Integer id;

	    @Column(name = "cate_init",nullable = false,unique = true)
	    @Size(max=3)
	    private String cateInit;

	    @Column(name = "cate_name")
	    @Size(max = 25)
	    private String cateName;

	    @Column(name = "cate_is_active")
	    @Size(max = 3)
	    private String cateIsActive;
	    
	    //join table
	    @OneToMany(mappedBy="categoryModel", cascade = CascadeType.ALL,fetch=FetchType.LAZY)
	    @JsonManagedReference
	    @Column(nullable=true)
	    private Set<VariantModel> variants;
	    

	    //Gater Seter 
		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}
		
		public String getCateInit() {
			return cateInit;
		}

		public void setCateInit(String cateInit) {
			this.cateInit = cateInit;
		}

		public String getCateName() {
			return cateName;
		}

		public void setCateName(String cateName) {
			this.cateName = cateName;
		}

		public String getCateIsActive() {
			return cateIsActive;
		}

		public void setCateIsActive(String cateIsActive) {
			this.cateIsActive = cateIsActive;
		}

	

	    
	    
	    
}
