package com.batch23x.models.dto;



public class ProductVariantDto {
	
	private Integer id;
	private String prodName;
	private double prodPrice;
	private Integer prodStock;
	private String varnName;
	private String cateName;
	
	//harus pake constructor
	public ProductVariantDto(Integer id, String prodName, double prodPrice, Integer prodStock, String varnName,
			String cateName) {
		super();
		this.id = id;
		this.prodName = prodName;
		this.prodPrice = prodPrice;
		this.prodStock = prodStock;
		this.varnName = varnName;
		this.cateName = cateName;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public double getProdPrice() {
		return prodPrice;
	}
	public void setProdPrice(double prodPrice) {
		this.prodPrice = prodPrice;
	}
	public Integer getProdStock() {
		return prodStock;
	}
	public void setProdStock(Integer prodStock) {
		this.prodStock = prodStock;
	}
	public String getVarnName() {
		return varnName;
	}
	public void setVarnName(String varnName) {
		this.varnName = varnName;
	}
	public String getCateName() {
		return cateName;
	}
	public void setCateName(String cateName) {
		this.cateName = cateName;
	}

	
	
	
	
	
	

}
