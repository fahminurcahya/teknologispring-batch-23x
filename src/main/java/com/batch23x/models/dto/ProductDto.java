package com.batch23x.models.dto;

public class ProductDto {
	
	private String prodName;
	private Integer prodStock;
	public ProductDto(String prodName, Integer prodStock) {
		super();
		this.prodName = prodName;
		this.prodStock = prodStock;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public Integer getProdStock() {
		return prodStock;
	}
	public void setProdStock(Integer prodStock) {
		this.prodStock = prodStock;
	}
	
	


}
