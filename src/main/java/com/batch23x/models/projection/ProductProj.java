package com.batch23x.models.projection;

public interface ProductProj {
	
	
//	Sesuai sama nama kolom, name, karena ada get jadi getName
	
	String getName();
	String getPrice();
	String getStock();
	Integer getVariantId();
	

}
