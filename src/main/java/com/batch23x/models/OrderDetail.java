package com.batch23x.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

@Entity
@Table(name="order_detail")
@Data
public class OrderDetail {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ordt_id")
	private Long ordtId;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="price")
	@Digits(integer = 8, fraction = 2)
	private Double price;
	
	@Column(name = "ordt_is_active")
	@Size(max = 1)
	private String isActive;
	
	@ManyToOne
	@JoinColumn(name = "ordt_order_id")
	@JsonBackReference
	private OrderHeader orderHeader;


}
