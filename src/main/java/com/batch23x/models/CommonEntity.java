package com.batch23x.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class CommonEntity {
	@Column(name = "created_on", nullable = true)
    protected LocalDateTime createdOn;
 
    @Column(name = "created_by", nullable = true)
    protected String createdBy;

    @Column(name = "modified_on",nullable = true)
    protected LocalDateTime modifiedOn;
    
    @Column(name = "modified_by",nullable = true)
    protected String modifiedBy;

    @Column(name = "delete_on",nullable = true)
    protected LocalDateTime deleteOn;
    
    @Column(name = "delete_by",nullable = true)
    protected String deletedBy;

    @Column(name = "is_delete", nullable = true)
    protected Boolean isDelete = false;

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public LocalDateTime getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(LocalDateTime deleteOn) {
		this.deleteOn = deleteOn;
	}

	public String getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}
    
    


}
