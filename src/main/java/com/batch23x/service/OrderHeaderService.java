package com.batch23x.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.batch23x.models.OrderHeader;

public interface OrderHeaderService {

		Iterable<OrderHeader> findAllOrder(Integer pageNo, Integer pageSize, String sortBy);
		Iterable<OrderHeader> findAll(Integer pageNo, Integer pageSize, String sortBy);
		Iterable<OrderHeader> findByActive(Integer pageNo, Integer pageSize, String sortBy);


}
