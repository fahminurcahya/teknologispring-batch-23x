package com.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batch23x.models.VariantModel;
import com.batch23x.repository.VariantRepo;
import com.batch23x.service.VariantService;

@Service
@Transactional
public class VariantServiceImpl implements VariantService  {

	@Autowired
	private VariantRepo varRep;
	
	@Override
	public List<VariantModel> findAllVariant() {
		// TODO Auto-generated method stub
		return varRep.findAll();
	}

	@Override
	public VariantModel save(VariantModel varMod) {
		// TODO Auto-generated method stub
		return varRep.save(varMod);
	}

	@Override
	public void delete(Integer id) {
		varRep.deleteById(id);;
		// TODO Auto-generated method stub
		
	}

	@Override
	public Optional<VariantModel> findById(Integer id) {
		// TODO Auto-generated method stub
		return varRep.findById(id);
	}

	@Override
	public List<VariantModel> findByIsActive(String isActive) {
		// TODO Auto-generated method stub
		return varRep.findByIsActive(isActive);
	}

	@Override
	public int updateVariantActive(String isActive, Integer varnId) {
		// TODO Auto-generated method stub
		return varRep.updateVariantActive(isActive, varnId);
	}

	@Override
	public int setVariantActive(String isActive, Integer varnId) {
		// TODO Auto-generated method stub
		return varRep.setVariantActive(isActive, varnId);
	}

	@Override
	public List<VariantModel> findVariantById(Integer id) {
		// TODO Auto-generated method stub
		return varRep.findVariantById(id);
	}

	@Override
	public List<VariantModel> findVarian(Integer id) {
		// TODO Auto-generated method stub
		return varRep.findVarian(id);
	}

	

}
