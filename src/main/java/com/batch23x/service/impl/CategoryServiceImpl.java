package com.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.batch23x.models.CategoryModel;
import com.batch23x.repository.CategoryRepo;
import com.batch23x.service.CategoryService;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepo cateRep;

	@Override
	public List<CategoryModel> findAllCategory() {
		// TODO Auto-generated method stub
		return cateRep.findAll();
	}

	@Override
	public CategoryModel save(CategoryModel cateMod) {
		// TODO Auto-generated method stub
		return cateRep.save(cateMod);
	}

	@Override
	public Optional<CategoryModel> findById(Integer id) {
		// TODO Auto-generated method stub
		return cateRep.findById(id);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		cateRep.deleteById(id);
		
	}

	@Override
	public List<CategoryModel> findByIsActive(String isActive) {
		// TODO Auto-generated method stub
		return cateRep.findByIsActive(isActive);
	}

	@Override
	public int updateCategoryActive(String isActive, Integer cateId) {
		// TODO Auto-generated method stub
		return cateRep.updateCategoryActive(isActive, cateId);
	}

	

	@Override
	public int setCategoryActive(String isActive, Integer cateId) {
		// TODO Auto-generated method stub
		return cateRep.setCategoryActive(isActive, cateId);
	}


	
	


}
