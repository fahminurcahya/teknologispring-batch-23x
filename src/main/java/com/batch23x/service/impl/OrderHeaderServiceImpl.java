package com.batch23x.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.batch23x.models.OrderHeader;
import com.batch23x.repository.OrderHeaderRepo;
import com.batch23x.service.OrderHeaderService;

@Service
public class OrderHeaderServiceImpl implements OrderHeaderService {
	
	@Autowired
	OrderHeaderRepo orderHeaderRepo;
		
	@Override
	public Iterable<OrderHeader> findAllOrder(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
		Page<OrderHeader> pageResult = orderHeaderRepo.findAll(paging);
		return pageResult.getContent();
	}

	@Override
	public Iterable<OrderHeader> findByActive(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo,pageSize, Sort.by(sortBy));
		Page<OrderHeader> pageResult = orderHeaderRepo.findByActive(paging);
		return pageResult.getContent();
	}


	@Override
	public Iterable<OrderHeader> findAll(Integer pageNo, Integer pageSize, String sortBy) {
		Pageable paging = PageRequest.of(pageNo,pageSize, Sort.by(sortBy));
		Page<OrderHeader> pageResult = orderHeaderRepo.findAll(paging);
		return pageResult.getContent();
	}


	

}
