package com.batch23x.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.batch23x.models.ProductModel;
import com.batch23x.models.dto.ProductDto;
import com.batch23x.models.dto.ProductVariantDto;
import com.batch23x.models.projection.ProductProj;
import com.batch23x.repository.ProductRepo;
import com.batch23x.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductRepo prodRep;
	
	@Override
	public List<ProductModel> findAllProduct() {
		// TODO Auto-generated method stub
		return prodRep.findAll();
	}

	@Override
	public ProductModel save(ProductModel prodMod) {
		// TODO Auto-generated method stub
		return prodRep.save(prodMod);
	}

	@Override
	public void delete(Integer id) {
		prodRep.deleteById(id);
		
	}

	@Override
	public Optional<ProductModel> findById(Integer id) {
		// TODO Auto-generated method stub
		return prodRep.findById(id);
	}

	@Override
	public List<ProductProj> fetchProduct() {
		// TODO Auto-generated method stub
		return prodRep.fetchProduct();
	}

	@Override
	public List<ProductDto> fetchProductDto() {
		// TODO Auto-generated method stub
		return prodRep.fetchProductDto();
	}

	@Override
	public List<ProductModel> findByName(Integer varnId,Integer pageNo, Integer pageSize, String sortBy) {
		Pageable pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy));
		Page<ProductModel> pageResult = prodRep.findByName(varnId, pageable);
		return pageResult.getContent();

	}

	@Override
	public List<ProductVariantDto> fecthProductVariant(Integer pageNo, Integer pageSize, String sortBy,
			String sortType) {
			Pageable pageable=null;
			if (sortType.equalsIgnoreCase("desc")) {
				pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).descending());
			}else {
				pageable = PageRequest.of(pageNo,pageSize,Sort.by(sortBy).ascending());
			}
			
			Page<ProductVariantDto> pageResult = prodRep.fetchProductVariant(pageable);
			return pageResult.getContent();

	}	
	

}
