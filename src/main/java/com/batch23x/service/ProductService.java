package com.batch23x.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.batch23x.models.ProductModel;
import com.batch23x.models.dto.ProductDto;
import com.batch23x.models.dto.ProductVariantDto;
import com.batch23x.models.projection.ProductProj;

public interface ProductService {
	List<ProductModel> findAllProduct();
	ProductModel save(ProductModel prodMod);
	void delete(Integer id);
	Optional<ProductModel>  findById(Integer id);
	List<ProductProj> fetchProduct();
	List<ProductDto> fetchProductDto();
	List<ProductModel> findByName(Integer varnId,Integer pageNo, Integer pageSize, String sortBy);
	List<ProductVariantDto> fecthProductVariant(Integer pageNo, Integer pageSize, String sortBy,String sortType);

}
