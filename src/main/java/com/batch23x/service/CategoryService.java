package com.batch23x.service;

import java.util.List;
import java.util.Optional;


import com.batch23x.models.CategoryModel;

public interface CategoryService {
	
	List<CategoryModel> findAllCategory();
	CategoryModel save(CategoryModel cateMod);
//	id disini sebagai parameter saja, nama bebas
	Optional<CategoryModel>  findById(Integer id);
	void delete(Integer id);
	List<CategoryModel> findByIsActive(String isActive);
	int updateCategoryActive(String isActive, Integer cateId);
	int setCategoryActive(String isActive, Integer cateId);
	

}
