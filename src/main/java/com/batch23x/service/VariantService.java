package com.batch23x.service;

import java.util.List;
import java.util.Optional;

import com.batch23x.models.VariantModel;

public interface VariantService {
	List<VariantModel> findAllVariant();
	VariantModel save(VariantModel varMod);
	void delete(Integer id);
	Optional<VariantModel>  findById(Integer id);
	List<VariantModel> findByIsActive(String isActive);
	int updateVariantActive(String isActive, Integer varnId);
	int setVariantActive(String isActive, Integer varnId);
	List<VariantModel> findVariantById(Integer id);
	List<VariantModel> findVarian(Integer id);
}
