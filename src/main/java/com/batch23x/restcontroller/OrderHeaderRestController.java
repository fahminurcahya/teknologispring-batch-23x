package com.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.batch23x.service.OrderHeaderService;

@RestController
@RequestMapping(path = "/api/order", produces = "application/json")
@CrossOrigin(origins = "*")
public class OrderHeaderRestController {
	
	@Autowired
	private OrderHeaderService orderHeaderSer;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllOrder(@RequestParam Integer pageNo, @RequestParam Integer pageSize, @RequestParam String sortBy) {
		return new ResponseEntity<>(orderHeaderSer.findAllOrder(pageNo, pageSize, sortBy) , HttpStatus.OK);
	}
	
	@GetMapping("/findByActive")
	public ResponseEntity<?> findByActive(@RequestParam Integer pageNo, @RequestParam Integer pageSize, @RequestParam String sortBy) {

		return new ResponseEntity<>(orderHeaderSer.findByActive(pageNo, pageSize, sortBy), HttpStatus.OK);
	}

	@GetMapping("/findAll")
	public ResponseEntity<?> finAll(@RequestParam Integer pageNo, @RequestParam Integer pageSize, @RequestParam String sortBy) {

		return new ResponseEntity<>(orderHeaderSer.findAll(pageNo, pageSize, sortBy), HttpStatus.OK);
	}

}
