package com.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.batch23x.models.CategoryModel;
import com.batch23x.service.CategoryService;

@RestController
@RequestMapping(path = "/api/category", produces = "application/json")
@CrossOrigin(origins = "*")
public class CategoryRestController {
	
	@Autowired
	private CategoryService cateSer;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllCategory() {
		return new ResponseEntity<>(cateSer.findAllCategory(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveCategory(@RequestBody CategoryModel category) {
		return new ResponseEntity<>(cateSer.save(category), HttpStatus.OK);
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findCategoryById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(cateSer.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> putCategory(@RequestBody CategoryModel category) {
		return new ResponseEntity<>(cateSer.save(category), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteCategory(@PathVariable("id") Integer id) {
		try {
			cateSer.delete(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
//	==================== Selasa 28 Jan 2020 =============================================================
	//custom JPA dengan JPQL
	@GetMapping("/findByIsActive/{isActive}")
	public ResponseEntity<?> findByIsActive(@PathVariable("isActive") String isActive) {
		return new ResponseEntity<>(cateSer.findByIsActive(isActive), HttpStatus.OK);
	}
	
	@PutMapping("/updateCategoryActive/{isActive}/{cateId}")
	public ResponseEntity<?> updateCategoryActive(@PathVariable("isActive") String isActive, @PathVariable("cateId") Integer cateId){
		return new ResponseEntity<>(cateSer.updateCategoryActive(isActive, cateId), HttpStatus.OK);
	}
	
	@PutMapping("/setCategoryActive")
	public ResponseEntity<?> setCategoryActive(@RequestParam("isActive") String isActive, @RequestParam ("cateId") Integer cateId){
		cateSer.setCategoryActive(isActive, cateId);
		return new ResponseEntity<>(cateSer.findById(cateId),HttpStatus.OK);
	}
	
	
	
	
	
	


}
