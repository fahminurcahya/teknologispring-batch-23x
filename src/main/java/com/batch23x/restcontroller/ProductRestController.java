package com.batch23x.restcontroller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.batch23x.models.ProductModel;
import com.batch23x.service.ProductService;

@RestController
@RequestMapping(path = "/api/product", produces = "application/json")
@CrossOrigin(origins = "*")
public class ProductRestController {
	@Autowired
	private ProductService prodSer;
	
	@GetMapping("/")
	public ResponseEntity<?> findAllProduct() {
		return new ResponseEntity<>(prodSer.findAllProduct(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveProduct(@RequestBody ProductModel product) {
		return new ResponseEntity<>(prodSer.save(product), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProduct(@PathVariable("id") Integer id) {
		try {
			prodSer.delete(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
	@GetMapping("/{id}") //id disini harus sama dengan pathvariable
	public ResponseEntity<?> findProductById(@PathVariable("id") Integer id) {
		return new ResponseEntity<>(prodSer.findById(id), HttpStatus.OK);
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> putVariant(@RequestBody ProductModel product) {
		return new ResponseEntity<>(prodSer.save(product), HttpStatus.OK);
	}
	@GetMapping("/fetchProduct")
	public ResponseEntity<?> fetchProduct() {
		return new ResponseEntity<>(prodSer.fetchProduct(), HttpStatus.OK);
	}
	@GetMapping("/fetchProductDto")
	public ResponseEntity<?> fetchProductDto() {
		return new ResponseEntity<>(prodSer.fetchProductDto(), HttpStatus.OK);
	}
	
	@GetMapping("/findByName")
	public ResponseEntity<?> findByName(@RequestParam Integer varnId,@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy) {
		return new ResponseEntity<>(prodSer.findByName(varnId, pageNo, pageSize, sortBy), HttpStatus.OK);
	}
	
	@GetMapping("/fecthProductVariant")
	public ResponseEntity<?> fecthProductVariant(@RequestParam Integer pageNo,
			@RequestParam Integer pageSize, @RequestParam String sortBy,@RequestParam String sortType) {
		
		return new ResponseEntity<>(prodSer.fecthProductVariant(pageNo, pageSize, sortBy,sortType), HttpStatus.OK);
	}





}
