package com.batch23x.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.batch23x.models.VariantModel;
import com.batch23x.service.VariantService;

@RestController
@RequestMapping(path = "/api/variant", produces = "application/json")
@CrossOrigin(origins = "*")
public class VariantRestController {

		@Autowired
		private VariantService variantSer;

		@GetMapping("/")
		public ResponseEntity<?> findAllVariant() {
			return new ResponseEntity<>(variantSer.findAllVariant(), HttpStatus.OK);
		}
		
		@PostMapping("/add")
		public ResponseEntity<?> saveVariant(@RequestBody VariantModel variant) {
			return new ResponseEntity<>(variantSer.save(variant), HttpStatus.OK);
		}
		
		@DeleteMapping("/delete/{id}")
		@ResponseStatus(HttpStatus.NO_CONTENT)
		public void deleteVariant(@PathVariable("id") Integer id) {
			try {
				variantSer.delete(id);
			} catch (EmptyResultDataAccessException e) {
				// TODO: handle exception
			}
		}
		
		@GetMapping("/{id}") //id disini harus sama dengan pathvariable
		public ResponseEntity<?> findVariantById(@PathVariable("id") Integer id) {
			return new ResponseEntity<>(variantSer.findById(id), HttpStatus.OK);
		}
		
		@PutMapping("/put")
		public ResponseEntity<?> putVariant(@RequestBody VariantModel variant) {
			return new ResponseEntity<>(variantSer.save(variant), HttpStatus.OK);
		}
		
		
		@GetMapping("/findByIsActive/{isActive}")
		public ResponseEntity<?> findByIsActive(@PathVariable("isActive") String isActive) {
			return new ResponseEntity<>(variantSer.findByIsActive(isActive), HttpStatus.OK);
		}
		
		@PutMapping("/updateVariantActive/{isActive}/{varnId}")
		public ResponseEntity<?> updateVariantActive(@PathVariable("isActive") String isActive, @PathVariable("varnId") Integer varnId){
			return new ResponseEntity<>(variantSer.updateVariantActive(isActive, varnId), HttpStatus.OK);
		}
		
		@PutMapping("/setVariantActive")
		public ResponseEntity<?> setVariantActive(@RequestParam("isActive") String isActive, @RequestParam ("varnId") Integer varnId){
			variantSer.setVariantActive(isActive, varnId);
			return new ResponseEntity<>(variantSer.findById(varnId),HttpStatus.OK);
		}
		
		@GetMapping("/findVarById/{id}")
		public ResponseEntity<?>  findVarById(@PathVariable ("id") Integer id) {
			return new ResponseEntity<>(variantSer.findVariantById(id) , HttpStatus.OK);
		}
		
		@GetMapping("/findVarian/{id}")
		public ResponseEntity<?>  findVarian(@PathVariable ("id") Integer id) {
			return new ResponseEntity<>(variantSer.findVarian(id) , HttpStatus.OK);
		}
		
		
		

}
